import csv
import os
import requests
from config import *
from datetime import datetime

mongo_client = pymongo.MongoClient(host=Config.MONGO_HOST,
                                   port=Config.MONGO_PORT)
db = mongo_client.get_database(Config.DB_NAME)

if not os.path.exists('us-zip-code-latitude-and-longitude.csv'):
    try:
        tz = requests.get("https://public.opendatasoft.com/explore/dataset/us-zip-code-latitude-and-longitude/download/?format=csv&timezone=Asia/Karachi&use_labels_for_header=true")
        open('us-zip-code-latitude-and-longitude.csv', 'wb').write(tz.content)
    except:
        quit()

db.zips.drop()
coll = db["zips"]
t1 = datetime.now()
with open("us-zip-code-latitude-and-longitude.csv") as fi:
    zf = csv.DictReader(fi, delimiter=";")
    batch = []
    for n, zaddr in enumerate(zf):
        zaddr["Latitude"] = float(zaddr["Latitude"])
        zaddr["Longitude"] = float(zaddr["Longitude"])
        zaddr["geopoint"] = {"type": "Point", "coordinates": [zaddr["Longitude"], zaddr["Latitude"]]}
        batch.append(zaddr)
        if n % 1000 == 0:
            print(n, zaddr)
        ret = coll.insert(batch)
        batch.clear()
print("Took", datetime.now() - t1)