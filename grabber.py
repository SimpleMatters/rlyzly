import json
import time
from random import shuffle

from seleniumwire import webdriver

from config import *


class ZillowGrabber:
    gecko_path = "/home/pavel/lib/geckodriver"
    search_url = "https://www.zillow.com/search/GetSearchPageState.htm"
    basic_url = "https://www.zillow.com/{0}-{1}-{2}/{3}"
    driver = None
    db = None

    def __init__(self):
        self.mongo_client = pymongo.MongoClient(host=Config.MONGO_HOST,
                                                port=Config.MONGO_PORT)
        self.db = self.mongo_client.get_database(Config.DB_NAME)
        self.driver = webdriver.Firefox(executable_path=self.gecko_path)
        self.driver.set_page_load_timeout(10)

    def __del__(self):
        self.driver.quit()

    def find_zips(self, city_name: str) -> list:
        return self.db.zips.find({"City": city_name}, {"_id": 0, "Zip": 1, "City": 1, "State": 1})

    def find_zips_in_geosphere(self, coords: list, dist: float):
        return self.db.zips.find({"geopoint": {"$geoWithin": { "$centerSphere": [coords, dist]}}},
                                 {"_id": 0, "Timezone": 0, "Daylight savings time flag": 0})

    def __cap_time(self, min_t, max_t = 60):
        try:
            res = self.driver.find_element_by_xpath('//h1[@class="search-title"]')
            time.sleep(min_t)
        except:
            time.sleep(max_t)  # we're redirected to captcha page

    def get_zip_ads(self, zip_code: str, mode: str = "rentals"):
        """
        get all ads for given ZIP code from Zillow.com
        :param zip_code: int, postal ZIP code
        :param mode: ["rentals" | "homes"]
        :return: list of jsons or []
        """
        z = self.db.zips.find_one({"Zip": zip_code}, {"_id": 0, "Zip": 1, "City": 1, "State": 1})
        print("Getting zip:", z)
        self.driver.get(self.basic_url.format(z["City"].lower().replace(" ", "-"),
                                              z["State"].lower(),
                                              z["Zip"],
                                              mode))
        self.__cap_time(2)

        target_reqs = [r for r in self.driver.requests
                       if "GetSearchPageState" in r.path
                       and r.response
                       and r.response.headers.get("Content-Type")
                       and "application/json" in r.response.headers.get("Content-Type")]
        print("Search count:", len(target_reqs))

        # for r in target_reqs:
        #     print(len(r.response.body), r.path)
        #     print(r.response.body)

        for request in reversed(target_reqs):
            # print(request.path)
            data = ""
            try:
                resp = request.response.body.decode("utf-8")
                data = json.loads(resp)
            except Exception as e:
                print(e)
                print(request.response.body)
            self.driver.requests.clear()
            try:
                return data.get("searchResults").get("mapResults")
            except:
                return []

        return []

    def collect(self, zips: list, mode: str="rentals"):
        """
        Collect all ads of one sort for a list of zip records and store them in mongodb
        :param zips: list of zips
        :param mode: ["rentals" | "homes"]
        :return:
        """
        ho = self.db[mode]
        for n, one in enumerate(zips, 1):
            listing = z.get_zip_ads(one, mode=mode)
            print(n, "/", len(zips), ">", len(listing))
            if listing:
                ho.insert(listing)
            else:
                print("Nothing to insert, may be due to perimeterx")

            self.__cap_time(12)


if __name__ == "__main__":
    z = ZillowGrabber()
    #zips = list(z.find_zips("Pompano Beach"))
    near_sunny_isles = [-80.142, 25.945]
    radius = 15 / 6378.1    # km divided by earth radius in km = search radius in radians
    zips = list(z.find_zips_in_geosphere(near_sunny_isles, radius))
    zips = [e["Zip"] for e in zips]
    shuffle(zips)
    print("Zips found:", len(zips))
    #print(json.dumps(zips, indent=2))

    #z.collect(zips, "rentals")
    z.collect(zips, "homes")
    #z.db["homes"].insert(z.get_zip_ads("33180", "homes"))
    z.driver.quit()
